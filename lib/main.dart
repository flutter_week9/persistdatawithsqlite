import 'dog.dart';
import 'dog_dao.dart';
import 'cat.dart';
import 'cat_dao.dart';

void main() async {
  var fido = Dog(
    id: 0,
    name: 'Fido',
    age: 35,
  );
  var dido = Dog(
    id: 1,
    name: 'Dido',
    age: 20,
  );
  await DogDao.insertDog(fido);
  await DogDao.insertDog(dido);

  print(await DogDao.dogs());

  fido = Dog(
    id: fido.id,
    name: fido.name,
    age: fido.age + 7,
  );
  await DogDao.updateDog(fido);
  print(await DogDao.dogs());

  await DogDao.deleteDog(0);
  print(await DogDao.dogs());

  var aido = Cat(
    id: 0,
    name: 'Aido',
    age: 12,
  );
  var rido = Cat(
    id: 1,
    name: 'Rido',
    age: 35,
  );
  await CatDao.insertCat(aido);
  await CatDao.insertCat(rido);

  print(await CatDao.cats());

  aido = Cat(
    id: aido.id,
    name: aido.name,
    age: aido.age + 9,
  );
  await CatDao.updateCat(aido);
  print(await CatDao.cats());

  await CatDao.deleteCat(0);
  print(await CatDao.cats());
}
